/* showvike@lapy:/mnt/4E72ECF272ECDFA7/Programmes/kidsAndCartoons_Projects/miniProjectOne_BillingSystemApplication$

   This project is completed by !n-f!n!ty..

   getName(Showvike Mondal Ovi);
   getBatch(19th Batch);
   getClassRoll(CSE-143);
   getSection(C);
   getE-mail(showvike@gmail.com);
*/

#include <iostream>
#include <cstring>
#include <cstdio>

using namespace std;

class billingSystemApplication
{
	long long accountNumber;
	char customerName[80];
	char address[80];
	long long idNumber;
	long long dueBill;

public:
	billingSystemApplication();

	void getData(long long n, char *a, char *b, long long x, long long y);
	void putData();

    long long returnAccountNumber();
    long long returnIdNumber();
    char* returnCustomerName();

    void getAccountNumber(long long n);
    void getCustomerName(char *a);
    void getAddress(char *b);
    void getDueBill(long long y);

	~billingSystemApplication();
};

billingSystemApplication::billingSystemApplication()
{
	accountNumber = 0;
	customerName[80] = '\n';
	address[80] = '\n';
	idNumber = 0;
	dueBill = 0;
}

void billingSystemApplication::getData(long long n, char *a, char *b, long long x, long long y)
{
	accountNumber = n;
	strcpy(customerName, a);
	strcpy(address, b);
	idNumber = x;
	dueBill = y;
}

void billingSystemApplication::putData()
{
	cout << "                   >>    Account Number: " << accountNumber << endl;
	cout << "                   >>    Customer Name: " << customerName << endl;
	cout << "                   >>    Address: " << address << endl;
	cout << "                   >>    ID Number: " << idNumber << endl;
	cout << "                   >>    Due Bill: " << -1 * dueBill << endl << endl;
}

long long billingSystemApplication::returnAccountNumber()
{
    return accountNumber;
}

long long billingSystemApplication::returnIdNumber()
{
    return idNumber;
}

char* billingSystemApplication::returnCustomerName()
{
    return customerName;
}

void billingSystemApplication::getAccountNumber(long long n)
{
    accountNumber = n;
}

void billingSystemApplication::getCustomerName(char *a)
{
    strcpy(customerName, a);
}

void billingSystemApplication::getAddress(char *b)
{
    strcpy(address, b);
}

void billingSystemApplication::getDueBill(long long y)
{
    dueBill = y;
}

billingSystemApplication::~billingSystemApplication()
{
	;
}

billingSystemApplication customer[1000];

char mainMenu();

void inputData(long long i);
char inputAnotherData();

long long checkNmbr(long long n);
long long checkDBill(long long n);
long long checkAccNmbr(long long i, long long n);
long long checkIdNmbr(long long i, long long n);

char searchAnAccount();
long long searchByName(long long i);
long long searchById(long long i);
char editOrSearchAgain(long long i);
char editAnAccount();

void inputAccNmbr(long long i);
void inputCstmrNam(long long i);
void inputAdrs(long long i);
void inputDBill(long long i);

void wrongInput();

int main()
{
	cout << "      .....::: ||Billing System Application|| :::....." << endl;

    long long I, K;

    I = 0;

	while(1)
	{
		char ch;

		ch = mainMenu();

		if(ch == '1')
		{
			inputData(I);

            I = I + 1;

            while(1)
            {
                ch = inputAnotherData();

                if(ch == 'a')
                {
                    inputData(I);

                    I = I + 1;
                }
                
                else if(ch == 'b')
                {
                    cout << "                   ^_^ Returning to Main Menu ^_^" << endl << endl;

                    break;
                }

                else
                    wrongInput();
            }
		}

		else if(ch == '2')
		{
			if(I == 0)
            {
                cout << "            There are No Accounts. At First Add One." << endl << endl;

                continue;
            }

            cout << "           ||||||||||||  Total Accounts: " << I << " ||||||||||||  " << endl << endl;

            while(1)
            {
                ch = searchAnAccount();

                if(ch == 'a')
                {
                    K = searchByName(I);

                    while(1)
                    {
                        ch = editOrSearchAgain(K);

                        if(ch == '1')
                        {
                            while(1)
                            {
                                ch = editAnAccount();

                                if(ch == 'a')
                                    inputAccNmbr(K);

                                else if(ch == 'b')
                                    inputCstmrNam(K);

                                else if(ch == 'c')
                                    inputAdrs(K);

                                else if(ch == 'd')
                                    inputDBill(K);

                                else if(ch == 'e')
                                {
                                    cout << "                   +_+ Cancelling Edit......... +_+" << endl << endl;

                                    break;
                                }

                                else
                                    wrongInput();
                            }
                        }

                        else if(ch == '2')
                            K = searchByName(I);

                        else if(ch == '3')
                        {
                            cout << "                   ~_~ Returning to Search Menu ~_~" << endl << endl;

                            break;
                        }

                        else
                            wrongInput();
                    }
                }

                else if(ch == 'b')
                {
                    K = searchById(I);

                    while(1)
                    {
                        ch = editOrSearchAgain(K);

                        if(ch == '1')
                        {
                            while(1)
                            {
                                ch = editAnAccount();

                                if(ch == 'a')
                                    inputAccNmbr(K);

                                else if(ch == 'b')
                                    inputCstmrNam(K);

                                else if(ch == 'c')
                                    inputAdrs(K);

                                else if(ch == 'd')
                                    inputDBill(K);

                                else if(ch == 'e')
                                {
                                    cout << "                   +_+ Cancelling Edit......... +_+" << endl << endl;

                                    break;
                                }

                                else
                                    wrongInput();
                            }
                        }

                        else if(ch == '2')
                            K = searchById(I);

                        else if(ch == '3')
                        {
                            cout << "                   ~_~ Returning to Search Menu ~_~" << endl << endl;

                            break;
                        }

                        else
                            wrongInput();
                    }
                }

                else if(ch == 'c')
                {
                    cout << "                   ^_^ Returning to Main Menu ^_^" << endl << endl;

                    break;
                }

                else
                    wrongInput();
            }
		}

		else if(ch == '3')
		{
			cout << "                  |_m_| Thank You for Using |_m_|" << endl;

            break;
        }

        else
            wrongInput();
	}

	return 0;
}

char mainMenu()
{
	cout << "                        ~ Main Menu ~" << endl;
	cout << "==================================================================" << endl;
	cout << "=                  1.    Add an Account                          =" << endl;
	cout << "=                  2.    Search an Customer                      =" << endl;
	cout << "=                  3.    Exit                                    =" << endl;
	cout << "==================================================================" << endl << endl;
	cout << "                   =>    Enter Choice [1 - 3]: ";

	char c;

	cin >> c;
	cout << endl;

	return c;
}

void inputData(long long i)
{
	long long accNmbr, idNmbr, dBill;
	char cstmrNam[80], adrs[80];

	while(1)
    {
        cout << "                 >>    Enter Account Number: ";
	    cin >> accNmbr;

        long long x, y;

        x = checkNmbr(accNmbr);

        if(x == 1)
        {
            cout << endl << "      The Value Cannot Be Less Than Or Equal Zero. Try Again." << endl << endl;

            continue;
        }

        y = checkAccNmbr(i, accNmbr);

        if(y == 1)
        {
            cout << endl << "    This Account Number Already Owned by a Customer. Try Another." << endl << endl;

            continue;
        }

        else
            break;
    }

    getchar();

	cout << "                 >>    Enter Customer Name: ";
	cin.getline(cstmrNam, 80);

	cout << "                 >>    Enter Address: ";
	cin.getline(adrs, 80);

    while(1)
    {
        cout << "                 >>    Enter ID Number: ";
	    cin >> idNmbr;

        long long x, y;

        x = checkNmbr(idNmbr);

        if(x == 1)
        {
            cout << endl << "      The Value Cannot Be Less Than Or Equal Zero. Try Again." << endl << endl;

            continue;
        }

        y = checkIdNmbr(i, idNmbr);

        if(y == 1)
        {
            cout << endl << "    This ID Number Already Owned by a Customer. Try Another." << endl << endl;

            continue;
        }

        else
            break;
    }

	while(1)
    {
        cout << "                 >>    Enter Due Bill: ";
	    cin >> dBill;

        long long x;

        x = checkDBill(dBill);

        if(x == 1)
        {
            cout << endl << "      The Value Cannot Be Less Than Zero. Try Again." << endl << endl;

            continue;
        }

        else
            break;
    }

	cout << endl;

	customer[i].getData(accNmbr, cstmrNam, adrs, idNmbr, dBill);
}

char inputAnotherData()
{
    cout << "------------------------------------------------------------------" << endl;
    cout << "-              Do You Want to Add Another Account?               -" << endl;
    cout << "-                       a.    Yes                                -" << endl;
    cout << "-                       b.    No                                 -" << endl;
    cout << "------------------------------------------------------------------" << endl << endl;
    cout << "                   ->    Enter Choice [a - b]: ";

    char c;
    
    cin >> c;
    cout << endl;

    return c;
}

long long checkNmbr(long long n)
{
    long long flag;

    if(n <= 0)
        flag = 1;

    return flag;
}

long long checkDBill(long long n)
{
    long long flag;

    if(n < 0)
        flag = 1;

    return flag;
}

long long checkAccNmbr(long long i, long long n)
{
    long long k, flag;

    for(k = 0; k < i; k = k + 1)
        if(customer[k].returnAccountNumber() == n)
        {
            flag = 1;

            break;
        }

    return flag;
}

long long checkIdNmbr(long long i, long long n)
{
    long long k, flag;

    for(k = 0; k < i; k = k + 1)
        if(customer[k].returnIdNumber() == n)
        {
            flag = 1;

            break;
        }

    return flag;
}

char searchAnAccount()
{
    cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    cout << "+                   How to Search an Account?                    +" << endl;
    cout << "+                      a.    By Name                             +" << endl;
    cout << "+                      b.    By ID                               +" << endl;
    cout << "+                      c.    Main Menu                           +" << endl;
    cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    cout << "                    ->    Enter Choice [a - c]: ";

    char c;
    
    cin >> c;
    cout << endl;

    return c;
}

long long searchByName(long long i)
{
    char a[80];
    long long k;

    getchar();

    while(1)
    {
        cout << "     >>>  Enter The Customer Name To Search: ";
        cin.getline(a, 80);

        cout << endl;

        for(k = 0; k < i; k = k + 1)
            if(strcmp(customer[k].returnCustomerName(), a) == 0)
            {
                customer[k].putData();

                return k;
            }

        if(k == i)
        {
            cout << "           " << a << " is Not Matched With Any Account. Try Again." << endl << endl;

            continue;
        }

        else
            break;
    }

    return 0;
}

long long searchById(long long i)
{
    long long n, k;

    while(1)
    {
        while(1)
        {
            cout << "     >>>  Enter The ID Number To Search: ";
            cin >> n;

            long long x;

            x = checkNmbr(n);

            if(x == 1)
            {
                cout << endl << "      The Value Cannot Be Less Than Or Equal Zero. Try Again." << endl << endl;

                continue;
            }

            else
                break;
        }

        cout << endl;

        for(k = 0; k < i; k = k + 1)
            if(customer[k].returnIdNumber() == n)
            {
                customer[k].putData();

                return k;
            }

        if(k == i)
        {
            cout << "           ID: " << n << " is Not Matched With Any Account. Try Again." << endl << endl;

            continue;
        }

        else
            break;
    }

    return 0;
}

char editOrSearchAgain(long long i)
{
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
    cout << "                 1.    Want to Edit " << customer[i].returnCustomerName() << "'s Information?" << endl;
    cout << "                 2.    Want to Search Again?" << endl;
    cout << "                 3.    Search Menu" << endl;
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
    cout << "                 =>    Enter Choice [1 - 3]: ";

    char c;

    cin >> c;
    cout << endl;

    return c;
}

char editAnAccount()
{
    cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" << endl;
    cout << "$                     What You Want to Edit?                     $" << endl;
    cout << "$                     a.    Account Number                       $" << endl;
    cout << "$                     b.    Customer Name                        $" << endl;
    cout << "$                     c.    Address                              $" << endl;
    cout << "$                     d.    Due Bill                             $" << endl;
    cout << "$                     e.    Cancel Edit                          $" << endl;
    cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$" << endl << endl;
    cout << "                    -> Enter Choice [a - e]: ";

    char c;

    cin >> c;
    cout << endl;

    return c;
}

void inputAccNmbr(long long i)
{
    long long n;

    while(1)
    {
        cout << "               ==>>    Enter New Account Number: ";
        cin >> n;

        long long x;

        x = checkNmbr(n);

        if(x == 1)
        {
            cout << endl << "      The Value Cannot Be Less Than Or Equal Zero. Try Again." << endl << endl;

            continue;
        }

        else
            break;
    }

    customer[i].getAccountNumber(n);

    cout << endl << "           -->>    After Edit -->>" << endl << endl;

    customer[i].putData();
}

void inputCstmrNam(long long i)
{
    char a[80];

    getchar();

    cout << "               ==>>    Enter New Customer Name: ";
    cin.getline(a, 80);

    customer[i].getCustomerName(a);

    cout << endl << "           -->>   After Edit -->>" << endl << endl;

    customer[i].putData();

}

void inputAdrs(long long i)
{
    char b[80];

    getchar();

    cout << "               ==>>    Enter New Address: ";
    cin.getline(b, 80);

    customer[i].getAddress(b);

    cout << endl << "           -->>    After Edit -->>" << endl << endl;

    customer[i].putData();
}

void inputDBill(long long i)
{
    long long n;

    while(1)
    {
        cout << "               ==>>    Enter New Due Bill: ";
        cin >> n;

        long long x;

        x = checkDBill(n);

        if(x == 1)
        {
            cout << endl << "      The Value Cannot Be Less Than Zero. Try Again." << endl << endl;

            continue;
        }

        else
            break;
    }

    customer[i].getDueBill(n);

    cout << endl << "           -->>    After Edit -->>" << endl << endl;

    customer[i].putData();
}

void wrongInput()
{
    cout << "                  -_-  Wrong Choice :/  -_-" << endl;
    cout << "                  *_*   Try Again   ;)  *_*" << endl << endl;
}

